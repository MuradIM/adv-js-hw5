import Card from '../classes/card2.js';
import filterUser from "./filterUser.js";

function showCards([users, posts]) {

    const usersFiltered = users.map(i => filterUser(i))

    const postsFiltered = usersFiltered.map(i => {
        let arr = []

        const id = i.id
        posts.map(index => {
            if (index.userId === id) {
                index.name = i.name
                index.email = i.email
                arr.push(index)
            }
        });
        return arr
    })
    let postsAndUsersBounded = []
    postsFiltered.map(i => {
        postsAndUsersBounded.push(...i)
    })

    postsAndUsersBounded.map(i => {
        const card = new Card(i)
        card.createCard()
        card.renderCard()
    })
    return postsAndUsersBounded
}

export default showCards