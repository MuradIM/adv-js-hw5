function getRequest(URL) {
    return fetch(`https://ajax.test-danit.com/api/json/${URL}`).then(response => response.json())
}

export default getRequest