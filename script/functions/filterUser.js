function filterUser(i) {
    return {
        name: i.name,
        email: i.email,
        id: i.id,
    }
}

export default filterUser