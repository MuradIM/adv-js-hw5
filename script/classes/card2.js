class Card {
    constructor({name, email, title, body, id}) {
        this.name = name
        this.email = email
        this.title = title
        this.body = body
        this.id = id
    }

    createCard() {
        this.div = document.createElement("div")
        this.div.innerHTML = `
            <div class="headline-wrapper">
                <h2 class="card__headline">${this.title}</h2>
                <button class="close-btn">x</button>
            </div>
            <div class="card__body">
                <p>${this.body}</p>
            </div>
            <div class="card__user">
                <p class="card__user--name">${this.name}</p>
                <p><a href="mailto:${this.email}">${this.email}</a></p>
            </div>
        `
        this.div.dataset.id = this.id
        this.div.addEventListener("click", this.cardRemoveBtn)
        this.div.classList.add("card")
        return this.div
    }

    renderCard() {
        const wrapper = document.querySelector(".cards-wrapper")
        wrapper.append(this.div)
    }

    cardRemoveBtn(e) {
        if (e.target.classList.contains("close-btn")) {
            const id = e.target.closest("[data-id]").dataset.id
            this.remove()
            fetch(`https://ajax.test-danit.com/api/json/posts/${id}`, {
                method: "DELETE"
            })
        }
    }
}

export default Card