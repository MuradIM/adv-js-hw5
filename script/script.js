import showCards from "./functions/showCards1.js";
import getRequest from "./functions/getRequest.js";

const [user, post] = [getRequest("users"), getRequest("posts")]

Promise.all([user, post])
    .then(showCards)